/*jshint multistr: true */
//Desabilitar console.log para aquellos navegadores en donde no funciona
if (!(window.console && console.log)) {
    (function() {
        var noop = function() {};
        var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'markTimeline', 'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'];
        var length = methods.length;
        var console = window.console = {};
        while (length--) {
            console[methods[length]] = noop;
        }
    }());
}
var Track = {
    envia: function(categoria,accion,etiqueta){
        console.log('send', 'event',categoria, accion, etiqueta);
        ga('send', 'event', categoria, accion, etiqueta);
    },
    pageview:function(pagina){
        console.log('send', 'pageview', pagina);
        ga('send', 'pageview', pagina);
    }
};

var banner = [
    {
        dir:"piso_flotante/",
        img:["piso_1.jpg","piso_2.jpg","piso_3.jpg"],
        textos:[
                {
                    h1:"Refleja tu estilo con la mayor variedad en pisos y muros.",
                    h2:"Descubre la mayor variedad en diseño y las últimas tendencias, con la calidad garantizada de Holztek"
                },
                {
                    h1:"DESCUBRE LAS ÚLTIMAS TENDENCIAS Y DISEÑOS PARA TUS PISOS Y MUROS.",
                    h2:"Descubre la mayor variedad en diseño y las últimas tendencias, con la calidad garantizada de Holztek"
                },
                {
                    h1:"RENUÉVATE CON LA MAYOR CALIDAD Y RESPALDO GARANTIZADO.",
                    h2:"Descubre la mayor variedad en diseño y las últimas tendencias, con la calidad garantizada de Holztek"
                }
                ],
        icono:"mano"
    }
];


var Front = {
    resizeListener : function(){
        this.w = $(window).width();
        this.h = $(window).height();

        var w = $("#over_slider_container").width();
        var wt = $("#over_slider_container svg").width();

        $("#over_slider_container").css("margin-left",((w-wt)*0.5)+"px");

        var wbox = $(".box-img img").width();
        $(".box-img .textos").css({"width":wbox+"px"});

        if(this.w <= 991 || this.h <= 900){
        } else {
        }
        
        if(this.w <= 767){
            
        }

        if(this.w >= 1300){
            var contw = $(".container").width();
            var dif = Math.floor((this.w -contw)*0.5) +15;
            $("#flecha_L").css({"left":dif+"px"});
            $("#flecha_R").css({"right":dif+"px"});
        }
    }
};

var Utils = {
    isMobile: function(){
        var isMobile = false; //initiate as false
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))){isMobile = true;} 
        
        return isMobile;
    },
    scroll:function(div,dif,time){
        var offset = div.offset();
        if(dif === undefined){
            dif = 0;
        }
        if(time === undefined){
            time = 1;
        }
        // console.log(offset.top);
        TweenMax.to(window,time,{scrollTo:{y:offset.top - dif}});
    }
};

var App = {
    init:function(){
        this.start();
        Front.resizeListener();
    },
    start:function(){
        this.setButtons();
        this.creaSlidesDetalle(0);
    },
    creaSlidesDetalle:function(j){ 
        var obj = banner[j];
        if($("#slider_detalle_"+j).length === 0){
            var st = '<div id="slider_detalle_'+j+'" class="slider_detalle" data-real-slider="'+Paletter.count+'">';
            for (var i = 0; i < obj.img.length; i++) {
                var img = obj.img[i];
                st +='<div class="slide" style="background-image:url(img/slider/'+(obj.dir+img)+')">\
                        <h1>'+obj.textos[i].h1+'</h1>\
                        <div class="linea-sep"></div>\
                        <div class="icon-cont">\
                            <div class="icono"><img src="img/iconos/'+obj.icono+'.png"></div>\
                        </div>\
                        <h2>'+obj.textos[i].h2+'</h2>\
                    </div>';
            }
            

            // st+='';
            $("#slider_detalle_container").append(st);

            var coloresBarra = ["#FFFFFF","#fc5001","#ffffff"];

            var clase ="slide";
            var id_contenedor ="slider_detalle_"+j.toString();
            Paletter.init(clase,id_contenedor,{index : $("#" +id_contenedor +" ."+clase).length -1, colores:coloresBarra, col_inactivo:"#fff",col_activo:"#fc5001"});
            // $("#slider_detalle_"+j +" .cont_dots").css({"z-index":obj.img.length+4});
        }
        
        this.muestraSlidesDetalle(j);
    },
    muestraSlidesDetalle:function(j){
        Paletter.pause();
        $(".slider_detalle").hide();
        $("#slider_detalle_"+j).show();
        var realSlider = $("#slider_detalle_"+j).attr("data-real-slider");
        Paletter.play(realSlider);
    },
    muestraSlidesPrincipal:function(){
        this.slidePrincipal = true;
        Paletter.pause();
        TweenMax.fromTo($("#slider_principal"), 0.5, {marginTop:Front.h*-1},{marginTop:0});
        TweenMax.fromTo($("#slider_detalle"), 0.5, {marginTop:0},{marginTop:Front.h, onComplete:function(){
            $(".slider_detalle").hide();
        }});
        
        Paletter.play(0);
    },
    resetAdicionalSlide:function(slide,i,j){
       
    },
    animaAdicionalSlide:function(slide,i,j){
        
    },
    setButtons:function(){
        $("#botonera_slides .btn").click(function(){
            window.open($(this).attr("data-url"),"_blank");
            // var i = $(this).attr("data");
            // var nombre = $(this).attr("data-name");
            // App.creaSlidesDetalle(i);
            // App.setCurrentBtn($(this));
        });

        // $( "#botonera_slides .btn:first" ).trigger( "click" );
        
        $("#flecha_L").click(function(){
            var n;
            $(".slider_detalle").each(function(i){
                if($(this).css("display") == "block"){
                    n = $(this).attr("data-real-slider");
                }
            });
            Paletter.mueveSlide(Paletter["paletter_"+n],"l");
        });
        $("#flecha_R").click(function(){
            var n;
            $(".slider_detalle").each(function(i){
                if($(this).css("display") == "block"){
                    n = $(this).attr("data-real-slider");
                }
            });
            Paletter.mueveSlide(Paletter["paletter_"+n],"r");
        });

        $(".box-img").each(function(i){
            var texto = $(this).find(".textos");
            var h1 = texto.find("h1");
            var h2 = texto.find("h2");
            var boton = texto.find(".btn-rojo");
            $(this).hover(
                function(){
                    TweenMax.to(texto,0.3,{height:"100%",backgroundColor:"rgba(0,0,0,0.7)", bottom:0});
                    TweenMax.to(h1,0.3,{color:"#ffffff",paddingTop:"37%",paddingBottom: "5px", textAlign: "center"});
                    TweenMax.to(h2,0.3,{color:"#ffffff", textAlign: "center"});
                    boton.show();
                    TweenMax.fromTo(boton,0.3,{marginTop:"40px", autoAlpha:0},{marginTop:"15px", autoAlpha:1});
                },
                function(){
                    TweenMax.to(texto,0.3,{height:"20%",backgroundColor:"rgba(255,255,255,0.4)", bottom:"15px"});
                    TweenMax.to(h1,0.3,{color:"#000000",paddingTop:"0%",paddingBottom: "0px", textAlign: "left"});
                    TweenMax.to(h2,0.3,{color:"#000000", textAlign: "left"});
                    boton.hide();
                    // TweenMax.fromTo(boton,0.3,{marginTop:"30px", autoAlpha:0},{marginTop:"0px", autoAlpha:1});
                }
            ).click(function(){
                window.open($(this).attr("data-url"),"_blank");
            });
        });
    },
    setCurrentBtn:function(btn){
        console.log("HOLA");
        $("#botonera_slides .btn").each(function(i){
            if($(this).hasClass("current")){
                $(this).removeClass("current");
            }
        });

        btn.addClass("current");
    }
};

// var Yutub = {
//     videoPlayer:null,
//     animando:false, //para los thumbs (si es que hay)
//     ponVideoYoutube:function(i, divVideo, arr){
//         /*TRACK*/
//         // track.click("video", arr[i].titulo);
//         /*******/
//         var st = '<iframe id="yt" class="yt" width="100%" height="100%" src="https://www.youtube.com/embed/'+arr[i]+'?rel=0&showinfo=0&wmode=opaque&modestbranding=1&enablejsapi=1&version=3&playerapiid=ytplayer&controls=0" frameborder="0" allowfullscreen></iframe>';
//         divVideo.html(st);
        
//         this.videoPlayer = new YT.Player("yt", {
//             events: {
//                 'onReady': Yutub.onPlayerReady,
//                 'onStateChange': Yutub.onPlayerStateChange
//             }
//         });
//     },
//     onPlayerReady: function(e) {
//         //console.log("ready?");
//         //e.target.playVideo();
//     },
//     onPlayerStateChange: function(e) {
//         if (e.data === 0) {
//             //console.log("final");
//         }
//         else if (e.data === 1) {
//             //console.log("play");
//         }
//         else if (e.data === 2) {
//            // console.log("pausa");
//         }
//     }
// };

// var YTready = false;
// function onYouTubeIframeAPIReady(){
//     console.log("YT READY");
//     YTready = true;
// }

$(function(){
    
    $(window).resize(function(){
        Front.resizeListener();
    });
    $(window).on("orientationchange", function(e) {
        Front.resizeListener();
    });
    // $(document).scroll(function() {
    //     Utils.checkScrollPos();
    // });
    App.init();
    
});